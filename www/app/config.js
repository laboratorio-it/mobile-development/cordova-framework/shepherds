(function () {
    'use strict';

    angular.module('entapp')
            .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {

        $stateProvider
                .state('home', {
                    url: '/home',
                    views: {
                        'header': {
                            templateUrl: "app/common/header.html",
                            controller: null,
                            controllerAs: 'vm'
                        },
                        'body': {
                            templateUrl: "app/home/home.html",
                            controller: 'HomeCtrl',
                            controllerAs: 'vm'
                        }
                    }
                })
                .state('login', {
                    url: '/',
                    views: {
                        'header': {
                            templateUrl: "app/common/header.html",
                            controller: null,
                            controllerAs: 'vm'
                        },
                        'body': {
                            templateUrl: "app/common/login.html",
                            controller: null,
                            controllerAs: null
                        }
                    }
                });

        $urlRouterProvider.otherwise('/');
    }
})();