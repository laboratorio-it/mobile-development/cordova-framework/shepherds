(function () {
    'use strict';

    angular.module('entapp.home')
            .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['$scope','$cordovaCapture', 'ngDialog'];

    function HomeCtrl($scope,$cordovaCapture, ngDialog) {
        var vm = this;

        vm.publications = [];
        /* Methods */
        vm.postSomething = postSomething;

        $scope.$on('$viewContentLoaded', function () {
             loadPublications();
        });

        function loadPublications() {
            vm.publications = JSON.parse(localStorage.getItem('publications')) || [];
        }

        function postSomething() {
            ngDialog.open({
                template: 'templates/list_post.html',
                scope: $scope
            }).closePromise.then(function (response) {
                if (response.value) {
                    switch (response.value) {
                        case 'text':
                            uploadPost({
                                type:'text'
                            });
                        break;
                        case 'picture':
                            captureImage();
                        break;
                        case 'video':
                            recordVideo();
                        break;
                        case 'audio':
                            recordAudio();
                        break;
                    }
                }   
            });
        }

        function captureImage() {
            var options = { limit: 1 };
            $cordovaCapture.captureImage(options).then(function(imageData) {
                var post = {
                    path : imageData[0].fullPath,
                    type : 'picture'
                }
                uploadPost(post);
                console.log(imageData);
            }, function(err) {
                console.log(err);
            });
        }

        function recordVideo() {
            var options = { limit: 1, duration: 15 };
            $cordovaCapture.captureVideo(options).then(function(videoData) {
                 var post = {
                    path : videoData[0].fullPath,
                    type : 'video'
                }
                uploadPost(post);
                 console.log(videoData);
            }, function(err) {
            // An error occurred. Show a message to the user
            });
        }

        function recordAudio() {
             var options = { limit: 1, duration: 10 };
            $cordovaCapture.captureAudio(options).then(function(audioData) {
                var post = {
                    path : audioData[0].fullPath,
                    type : 'audio'
                }
                console.log(audioData);
                uploadPost(post);
            }, function(err) {
            // An error occurred. Show a message to the user
            });
        }

        function uploadPost(post) {
            $scope.post = post;
            console.log(post);
            ngDialog.open({
                template: 'templates/upload_post.html',
                scope: $scope
            }).closePromise.then(function (response) {
                console.log(response);
                if (response.value) {
                    savePublication(response.value);
                }   
            });
        }
        
        function savePublication(publication) {
            vm.publications.push(publication);
            localStorage.setItem('publications',JSON.stringify(vm.publications));
            ngDialog.openConfirm({
                template:' \
                    <div style="text-align: center;">\
                        <p style="padding-top: 20px;padding-bottom: 20px;">Yay! Haz publicado</p>\
                        <div class="ngdialog-buttons">\
                            <button type="button" class="col-xs-12 ngdialog-button ngdialog-button-primary" ng-click="confirm(1)">Ok</button>\
                        </div>\
                    </div>',
                plain: true
            });
        }
        
    }

})();